docker network create external_network

for app_folder in Traefik Loki; do
  cd "$app_folder" || exit
  docker compose up -d
  cd ..
  sleep 2
done

for app_folder in Ghost Gitea Joomla Minio; do
  cd "$app_folder" || exit
  docker compose up -d
  cd ..
done