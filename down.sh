for app_folder in Ghost Gitea Joomla Minio Loki Traefik; do
  cd "$app_folder" || exit
  docker compose down
  cd ..
done