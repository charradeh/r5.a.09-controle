# Documentation du déploiement

## 1. Cloner le projet

Cloner le projet depuis l'URL du dépôt:

```bash
git clone https://gitlabinfo.iutmontp.univ-montp2.fr/charradeh/r5.a.09-controle
cd r5.a.09-controle
```

## 2. Configurer les Applications

Pour chaque application (Ghost, Joomla, Gitea, Minio), vérifiez et ajustez les paramètres spécifiques dans les fichiers docker-compose.yml situés dans les dossiers correspondants.

## 3. Convertissez les noms d'hôtes des stacks

Dans le fichier /etc/hosts sur Mac, C:\windows\system32\drivers\etc\hosts sur Windows, ajoutez les lignes suivantes:

```
127.0.0.1 grafana.local
127.0.0.1 joomla.local
127.0.0.1 ghost.local
127.0.0.1 gitea.local
127.0.0.1 minio.local
```

## 4. Lancer l'Infrastructure

```bash
chmod +x up.sh
```

```bash
./up.sh
```

## 5. Accéder aux Applications

- Traefik Dashboard : http://localhost:8080
- Grafana : http://grafana.local
- Minio : http://minio.local
- Joomla : http://joomla.local
- Ghost : http://ghost.local
- Gitea : http://gitea.local

## 6. Arrêter l'Infrastructure

```bash
chmod +x down.sh
```

```bash
./down.sh
```
